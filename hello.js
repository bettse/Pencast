
//Add hidden input button for selecting client side PDF
d3.select('body').append('input')
  .attr('id', 'fileInput')
  .attr('class', 'fileInput')
  .attr('type', 'file')
  .attr('style', 'visibility: hidden; position: fixed; right: 0; top: 0')
  .on('contextmenu', function(e) { e.preventDefault(); });

d3.select('#filename')
  .on('click', function(e) {
    d3.select('#fileInput')[0][0].click();
  });

var pageLoaded = function() {
  var pdfFilename = 'Livescribe.pdf';

  //Allow server pdf to be specified in url
  var hash = document.location.hash.substring(1);
  var hashParams = parseQueryString(hash);
  if (hashParams.file) {
    pdfFilename = hashParams.file;
  }

  var request = d3.xhr(pdfFilename).responseType('arraybuffer').get(function(error, xhr) {
    if (error) return console.warn(error);
    playPdfData(new Uint8Array(xhr.response), pdfFilename);
  });

}();

var playPdfData = function(data, filename) {
  d3.select('#filename').text(filename);
  var pdf = new PDFDocument(null, data);
  pdf.parseStartXRef();
  pdf.parse();
  var xref = pdf.xref, trailer = xref.trailer;
  var inkml = trailer.get('Root').get('Livescribe_Metadata').get('Livescribe_InkML');
  if(isStream(inkml)) {
    renderinkml(bytesToString(inkml.getBytes()), filename);
  }
};

var renderinkml = function(text, filename) {
  var width = 600, height = 800;
  var parser = new DOMParser();
  xml = parser.parseFromString( text, "text/xml" );
  var traceGroups = nodeListToArray(xml.documentElement.getElementsByTagName('traceGroup')).map(parseTracegroup);

  var pdfs = d3.select('#pdfs').selectAll('.pdf')
    .data([filename], function(d) { return d; });
  pdfs.enter().append("div").attr('class', 'pdf');
  pdfs.append('h1').text(filename);
  pdfs.exit().remove();

  var pages = pdfs.selectAll('svg').data(traceGroups, function(d) { return d.id; });
  pages.enter().append('svg');
  pages
    .attr('id', function(d) { return d.id; })
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr('viewBox', function(d) { return d.cropBounds; })
    .attr("width", width)
    .attr("height", height);

  pages.exit().remove();

  var zoomed = function(d, i) {
    d3.select(this).select('g').attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
  };

  var zoom = d3.behavior.zoom().scaleExtent([1, 10]).on("zoom", zoomed);
  pages.call(zoom);

  var strokes = pages.append('g')
    .selectAll("path").data(function(d) { return d.traces; });
  strokes.enter().append("path");
  strokes.exit().remove();
  strokes
    .on('click', function(d) { console.log(d.time); })
    .transition().delay(function(d) {return d.timeOffset;})
    .attr('class', 'line')
    .attr("d", function(d) { return d.path; });

};


var nodeListToArray = function(nl) {
  var arr = [];
  for(var i = nl.length; i--; arr.unshift(nl[i]));
  return arr;
};

var parseTracegroup = function(tg) {
  var id = tg.getAttribute('xml:id'),
  startTime = tg.getElementsByTagName('timestamp')[0].getAttribute('time'),
  activeArea = tg.getElementsByTagName('activeArea'), mediaSize, cropBounds;
  if(activeArea.length > 0) {
    activeArea = activeArea[0];
    mediaSize = activeArea.getAttribute('mediaSize').replace(/[()]/g, '').split(',');
    mediaSize = { width: mediaSize[0].trim(), height: mediaSize[1].trim() };
    cropBounds = activeArea.getAttribute('cropBounds').replace(/[{(,)}]/g, '');
  }
  var traces = nodeListToArray(tg.getElementsByTagName("trace")).map(function(t) {
    return {
      timeOffset: t.getAttribute('timeOffset'),
      time: t.getAttribute('timeOffset') + startTime,
      path: "M " + t.textContent.replace(/,/g, 'l')
    };
  });
  return {
    id: id,
    mediaSize: mediaSize,
    cropBounds: cropBounds,
    startTime: startTime,
    traces: traces
  };
};



function parseQueryString(query) {
  var parts = query.split('&');
  var params = {};
  for (var i = 0, ii = parts.length; i < parts.length; ++i) {
    var param = parts[i].split('=');
    var key = param[0];
    var value = param.length > 1 ? param[1] : null;
    params[decodeURIComponent(key)] = decodeURIComponent(value);
  }
  return params;
}

window.addEventListener('change', function webViewerChange(evt) {
  var files = evt.target.files;
  if (!files || files.length === 0) return;
  var file = files[0];

  // Read the local file into a Uint8Array.
  var fileReader = new FileReader();
  fileReader.onload = function webViewerChangeFileReaderOnload(evt) {
    var buffer = evt.target.result;
    var uint8Array = new Uint8Array(buffer);
    playPdfData(uint8Array, file.name);
  };

  fileReader.readAsArrayBuffer(file);
}, true);

