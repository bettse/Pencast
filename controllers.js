/* global angular, d3, PDFDocument, isStream, bytesToString, console */

angular.module('pencast.controllers', []).
  controller('AppCtrl', function AppCtrl ($scope, $http, $log) {
    // initialize the model
    $scope.filename = 'angular.pdf';
    var audioData;

    var nodeListToArray = function(nl) {
      var arr = [];
      for(var i = nl.length; i--; arr.unshift(nl[i]));
      return arr;
    };

    var parseAudioSession = function(as) {
      audioData.start = +as.getAttribute('start');
      audioData.end = +as.getAttribute('end');
    };

    var parseTracegroup = function(tg) {
      var startTime = +tg.getElementsByTagName('timestamp')[0].getAttribute('time');
      var activeArea = tg.getElementsByTagName('activeArea'), mediaSize, cropBounds;
      if(activeArea.length > 0) {
        activeArea = activeArea[0];
        mediaSize = activeArea.getAttribute('mediaSize').replace(/[()]/g, '').split(',');
        mediaSize = { width: mediaSize[0].trim(), height: mediaSize[1].trim() };
        cropBounds = activeArea.getAttribute('cropBounds').replace(/[{(,)}]/g, '');
      }
      var traces = nodeListToArray(tg.getElementsByTagName("trace")).map(function(t) {
        return {
          timeOffset: +t.getAttribute('timeOffset'),
          time: +t.getAttribute('timeOffset') + startTime,
          path: "M " + t.textContent.replace(/,/g, 'l')
        };
      });

      var traceGroup = {
        id: tg.getAttribute('xml:id'),
        mediaSize: mediaSize,
        cropBounds: cropBounds,
        startTime: startTime,
        traces: traces
      };

      return traceGroup;
    };

    var parseArrayBuffer = function(data) {
      var array = new Uint8Array(data);
      var pdf = new PDFDocument(null, array);
      pdf.parseStartXRef();
      pdf.parse();
      var xref = pdf.xref, trailer = xref.trailer;
      audioData = {};
      var rootRef = trailer.get('Root');
      if (rootRef.has('Names')) {
        var names = rootRef.get('Names');
        if(names.has('EmbeddedFiles')) {
          var embeddedFiles = names.get('EmbeddedFiles');
          if(embeddedFiles.has('Names')) {
            var embeddedNames = embeddedFiles.get('Names');
            audioData.name = embeddedNames[0];
            var audioStream = xref.fetch(embeddedNames[1]).get('EF').get('F');
            if(isStream(audioStream)) {
              audioData.base64 = btoa(bytesToString(audioStream.getBytes()));
            }
          }
        }
      }
      if(rootRef.has('Livescribe_Metadata')) {
        var livescribeMetadata = rootRef.get('Livescribe_Metadata');
          if(livescribeMetadata.has('Livescribe_InkML')) {
          var inkml = livescribeMetadata.get('Livescribe_InkML');
          if(isStream(inkml)) {
            var text = bytesToString(inkml.getBytes());
            var parser = new DOMParser();
            var xml = parser.parseFromString( text, "text/xml" );
            $scope.traceGroups = nodeListToArray(xml.documentElement.getElementsByTagName('traceGroup')).map(parseTracegroup);
            nodeListToArray(xml.documentElement.getElementsByTagName('audioSession')).map(parseAudioSession);
          }
        }
      }
    };

    $scope.getPdfData = function() {
      $http({
        method: 'GET',
        responseType: 'arraybuffer',
        url: $scope.filename
      }).
      success(function (data) {
        // attach this data to the scope
        parseArrayBuffer(data);
        $scope.audioData = audioData;
        // clear the error messages
        $scope.error = '';
      }).
      error(function (data, status) {
        if (status === 404) {
          $scope.error = 'That pdf does not exist';
        } else {
          $scope.error = 'Error: ' + status;
        }
      });
    };

    //Load the pdf data immediately
    $scope.getPdfData();
  });

