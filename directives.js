/* global angular, d3, console */
angular.module('pencast.directives', []).
  directive('pdfVisualization', function () {
    // constants
    var b64header = "data:audio/mp4;base64,";

    return {
      restrict: 'E',
      scope: {
        pdf: '=',
        pages: '=',
        audioData: '='
      },
      templateUrl: 'pdfVisualization.html',
      link: function (scope, element, attrs) {
        // set up initial svg object
        var vis = d3.select(element[0]).select('.pdf');
        var audioEl = element.find('audio')[0];
        var pages, strokes, start, zooms = [];

        scope.$watch('audioData', function (newVal, oldVal) {
          if (!newVal) { return; } // if 'val' is undefined, exit
          if(scope.audioData.hasOwnProperty('start')) {
            start = scope.audioData.start;
            var audioData = newVal;
            audioEl.className = '';//remove hidden class
            audioEl.src = b64header + audioData.base64;
            audioEl.load();
          } else {
            audioEl.className = 'hidden';
          }
        }, true);

        scope.redraw = function(d, i) {
          d3.select(this).select('g')
            .attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
          var shouldHideReset = zooms.reduce(function(prev, curr) { return prev && (curr.scale() <= 1); }, true);
          d3.select('.pencast').select('#resetZoom').classed('hidden', shouldHideReset);
        };

        scope.resetZoom = function() {
          zooms.map(function(zoom) {
            zoom.scale(1).translate([0, 0]).event(d3.selectAll('svg'));
          });
        };

        scope.$watch('pages', function (newVal, oldVal) {
          if (!newVal) { return; } // if 'val' is undefined, exit
          var traceGroups = newVal;

          // clear the elements inside of the directive
          vis.html('');
          vis.selectAll('*').remove();

          //I use an svg per page so I can scale using the viewBox
          pages = vis.selectAll('svg').data(traceGroups, function(d) { return d.id; });
          pages.enter().append('svg');
          pages
            .attr('id', function(d) { return d.id; })
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("width", 95/2 + '%')
            .attr('viewBox', function(d) { return d.cropBounds; });

          pages.each(function(d, i) {
            var zoom = d3.behavior.zoom().scaleExtent([1, 10]).on("zoom", scope.redraw);
            d3.select(this).call(zoom);
            zooms.push(zoom);
          });
          if(pages.size() != zooms.length) {
            console.log("Something is wrong, the number of pages doesn't equal the number of zoom behaviour functions", pages.size(), zooms.length);
          }

          pages = pages.append('g');

          strokes = pages.selectAll("path")
            .data(function(d) { return d.traces; }, function(d) { return d.time; })
            .enter().append("path")
              .attr('class', 'line')
              .attr("d", function(d) { return d.path; })
              .on('click', function(d) {
                var newTime = Math.max(0, (d.time - start) / 1000);
                console.log('Setting audio time to', newTime);
                audioEl.currentTime = newTime;
              });
        });

        element.find('audio').bind("timeupdate", function(){
          var currentTime = +this.currentTime * 1000 + start;
          return playStrokes(currentTime);
       });

        var playStrokes = function(currentTime) {
          strokes.classed('past', function(d, i) { return d.time <= currentTime; });
        };
      }
    };
  });

